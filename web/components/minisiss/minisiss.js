(function () {
    'use strict';

    angular
            .module('app.minisiss', [])
            .service('Minisiss', Minisiss);

    Minisiss.$inject = ['$http'];
    function Minisiss($http) {
        var service = {
            verifyUser: verifyUser,
            getTasks: getTasks,
            getScores: getScores,
            getUsers: getUsers,
            getSchedules: getSchedules,
            closeSesion: closeSesion
        };

        function verifyUser(user, onSuccess, onError) {
            $http({
                method: 'POST',
                url: 'http://localhost:8080/minisissII/api/user/',
                data: user
            }).then(onSuccess, onError);
        }

        function getTasks(onSuccess, onError) {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/minisissII/api/task/'
            }).then(onSuccess, onError);
        }

        function getScores(user, onSuccess, onError) {
            $http({
                method: 'POST',
                url: 'http://localhost:8080/minisissII/api/task/',
                data: user
            }).then(onSuccess, onError);
        }

        function getUsers(onSuccess, onError) {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/minisissII/api/user/'
            }).then(onSuccess, onError);
        }
        
        function getSchedules(onSuccess, onError) {
            $http
                .get('http://localhost:8080/minisissII/api/schedule/')
                .then(onSuccess, onError);
        }
        
        function closeSesion() {
            $http({
                method: 'POST',
                url: 'http://localhost:8080/minisissII/api/sesion/'
            });
        }
        return service;
    }

})();