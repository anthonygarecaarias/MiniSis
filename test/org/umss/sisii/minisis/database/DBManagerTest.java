/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umss.sisii.minisis.database;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.umss.sisii.minisis.model.Score;
import org.junit.BeforeClass;
import org.umss.sisii.minisis.model.User;
import static org.junit.Assert.*;
import org.umss.sisii.minisis.model.Schedule;

/**
 *
 * @author anthony
 */
public class DBManagerTest {

    private static UserCRUD userCRUD;
    private static int id;

    @BeforeClass
    public static void setUp() {
        id = 3;
        userCRUD =new UserCRUD();
        userCRUD.createSesion(id);
    }

    @Test
    public void testVerifyUser() {
        User expected = new User();
        User ale = new User();

        ale.setUserName("ale");
        ale.setUserPassword("ale");
        expected.setId(id);
        expected.setUserName("ale");
        expected.setUserPassword("ale");
        ale.setId(userCRUD.checkUser(ale).getId());

        assertEquals(expected, ale);
    }

    @Test
    public void testUserScore() {
        List<Score> expected = new ArrayList<>();
        Score first = new Score();
        first.setScore(36);
        first.setTask("tarea 3");
        Score second = new Score();
        second.setScore(62);
        second.setTask("tarea 13");
        expected.add(first);
        expected.add(second);
        assertEquals(expected, userCRUD.getScores());
    }

    @Test
    public void testGetUsers() {

        User firstUser = new User();
        firstUser.setId(1);
        firstUser.setFirstName("jhonatan");
        firstUser.setLastName("sanchez");
        firstUser.setEmail("jhonaumss@gmail.com");
        firstUser.setPhone(69493775);
        firstUser.setAge(21);
        firstUser.setCareer("Ing. Sistemas");

        User finalUser = new User();
        finalUser.setId(10);
        finalUser.setFirstName("daniela");
        finalUser.setLastName("fernandez");
        finalUser.setEmail("dfernandez@gmail.com");
        finalUser.setPhone(23769834);
        finalUser.setAge(20);
        finalUser.setCareer("Ing. Sistemas");

        List<User> users = userCRUD.getUsers();
        assertEquals(firstUser, users.get(0));
        assertEquals(finalUser, users.get(users.size()-1));
    }
    
    @Test
    public void testGetSchudeles(){
        List<Schedule> sch=new ArrayList<>();
        Schedule firstSch=new Schedule();
        firstSch.setDay("Martes");
        firstSch.setHour("11:15-12:45");
        firstSch.setClassroom("655");
        Schedule secondSch=new Schedule();
        secondSch.setDay("Miercoles");
        secondSch.setHour("11:15-12:45");
        secondSch.setClassroom("655");
        Schedule thirdSch=new Schedule();
        thirdSch.setDay("Viernes");
        thirdSch.setHour("06:45-08:15");
        thirdSch.setClassroom("690B");
        sch.add(firstSch);
        sch.add(secondSch);
        sch.add(thirdSch);
        
        assertEquals(sch, userCRUD.getSchedule());
    
    }
}
