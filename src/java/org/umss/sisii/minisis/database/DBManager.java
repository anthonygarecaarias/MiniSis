package org.umss.sisii.minisis.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {

    private static final String URL = "jdbc:postgresql://localhost:5432/minisiss";
    private static final String PASSWORD = "postgres";
    private static final String USER = "postgres";
    private static DBManager manager;
    private Connection connection;

    public static DBManager getInstance() {
        if (manager == null) {
            manager = new DBManager();
        }
        return manager;
    }

    private DBManager() {
        conectar();
    }

    private void conectar() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            if (connection != null) {
                System.out.println("Conectando a Base de Datos...");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problemas de Conexion");
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() {
        try {
            connection.close();
            connection = null;
            manager = null;
        } catch (SQLException e) {
            System.out.println("Fallo el cierre");
        }
    }
}
